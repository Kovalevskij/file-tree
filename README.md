# File Tree

This is file tree example.

## Getting started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node ^5.4.1, npm ^3.3.12
- [Bower](bower.io) (`npm install --global bower`)
- [Ruby](https://www.ruby-lang.org) and then `gem install sass`
- [Gulp](http://www.gulpjs.com/) (`npm install --global gulp-cli`)

### Developing

1. Run `npm install` to install server dependencies.

2. Run `bower install` to install front-end dependencies.

3. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

### Description

For folder tree task I used [angular-ui-tree](https://github.com/angular-ui-tree/angular-ui-tree) component.
Implemented folder tree with ability to add new folders, delete folders and showing current active folder.
