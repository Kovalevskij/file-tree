(function (ng) {
  'use strict';
  ng.module('fileTree')
    .controller('ModalController', ModalController);

  /** @ngInject */
  function ModalController($uibModalInstance) {
    var vm = this;

    angular.extend(vm, {
      ok: function () {
        $uibModalInstance.close(vm.folderName);
      },

      cancel: function () {
        $uibModalInstance.dismiss();
      }
    });
  }

})(angular);
