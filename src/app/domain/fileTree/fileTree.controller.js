(function (ng) {
  'use strict';
  ng.module('fileTree')
    .controller('FileTreeController', FileTreeController);

  /** @ngInject  */
  function FileTreeController($uibModal, FileTreeService) {
    var vm = this;

    function openModal() {
      return $uibModal.open({
        templateUrl: 'app/components/modal/modalTemplate.html',
        controller: 'ModalController',
        controllerAs: 'vm'
      }).result;
    }

    function collapseChildNodes(scope) {
      var children = scope.childNodes();
      if (children.length > 0) {
        children.forEach(function (child) {
          collapseChildNodes(child);
        });
      }
      children.forEach(function (node) {
        node.collapse();
      });
    }

    function createPath(scope, title) {
      if (!scope.$parentNodeScope) {
        return (title + scope.$modelValue.title).split('/').reverse().join('/');
      }
      else {
        title += scope.$modelValue.title + '/';
        return createPath(scope.$parentNodeScope, title);
      }
    }

    function setCurrentFolder(scope) {

      if (scope.collapsed && !scope.$parentNodeScope || Object.keys(scope).length === 0) {
        vm.currentFolder = '/';
      }
      else if (scope.collapsed) {
        vm.currentFolder = createPath(scope.$parentNodeScope, '/');
      } else {
        vm.currentFolder = createPath(scope, '/');
      }
    }

    angular.extend(vm, {
      data: FileTreeService.data,
      remove: function (scope) {
        scope.remove();
        setCurrentFolder(scope.$parentNodeScope || {});
      },
      currentFolder: '/',
      newSubItem: function (scope) {
        var nodeData = scope.$modelValue;
        openModal().then(function (folderName) {
          nodeData.nodes.push({
            id: nodeData.id * 10 + nodeData.nodes.length,
            title: folderName,
            nodes: []
          });
        });
      },
      addRootItem: function () {
        openModal().then(function (folderName) {
          vm.data.push({
            id: vm.data.length,
            title: folderName,
            nodes: []
          })
        });
      },
      toggle: function (scope) {
        if (!scope.collapsed) {
          collapseChildNodes(scope);
          scope.collapse();
        }
        else {
          scope.siblings().forEach(function (sibling) {
            sibling.collapse();
            collapseChildNodes(sibling);
          });
          scope.toggle();
        }
        setCurrentFolder(scope);
      }
    });
  }

})(angular);
