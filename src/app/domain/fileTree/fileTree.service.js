(function (ng) {
  'use strict';
  ng.module('fileTree')
    .service('FileTreeService', function () {
      return {
        data: [{
          'id': 1,
          'title': 'folder1',
          'nodes': [
            {
              'id': 11,
              'title': 'folder1.1',
              'nodes': [
                {
                  'id': 111,
                  'title': 'folder.1.1',
                  'nodes': []
                }
              ]
            },
            {
              'id': 12,
              'title': 'folder.2',
              'nodes': []
            }
          ]
        }, {
          'id': 2,
          'title': 'folder2',
          'nodes': [
            {
              'id': 21,
              'title': 'folder2.1',
              'nodes': []
            },
            {
              'id': 22,
              'title': 'folder2.2',
              'nodes': []
            }
          ]
        }, {
          'id': 3,
          'title': 'folder3',
          'nodes': [
            {
              'id': 31,
              'title': 'folder3.1',
              'nodes': []
            }
          ]
        }]
      }
    });

})(angular);
