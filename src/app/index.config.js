(function() {
  'use strict';

  angular
    .module('fileTree')
    .config(config);

  /** @ngInject */
  function config($logProvider, treeConfig) {
    // Enable log
    $logProvider.debugEnabled(true);

    treeConfig.defaultCollapsed = true;
  }

})();
