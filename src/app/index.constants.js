/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('fileTree')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
