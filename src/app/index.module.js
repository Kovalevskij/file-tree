(function() {
  'use strict';

  angular
    .module('fileTree', [
      'ui.router',
      'ui.bootstrap',
      'toastr',
      'ui.tree'
    ]);

})();
