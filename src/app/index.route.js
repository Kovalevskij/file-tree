(function () {
  'use strict';

  angular
    .module('fileTree')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/domain/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('fileTree', {
        url: '/fileTree',
        templateUrl: 'app/domain/fileTree/fileTree.html',
        controller: 'FileTreeController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
